# Movie APP

## Project Overview
This is the Assignment shared by Vedantu.

This app uses the API from [themoviedb.org](https://www.themoviedb.org/)

## API Key Note
**Define key in build.gradle**

Find a file named `gradle.properties` in `.gradle` folder in your home directory.

Add `API_KEY = "YOUR-API-KEY"` to that file.


## What I learned
- Fetch data from the Internet with theMovieDB API
- Use adapters and custom list layouts to populate list views
- Incorporate libraries to simplify the amount of code you need to write
- Build a fully featured application that looks and feels natural on the latest Android operating system.

## Libraries
- [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/)
    * [Paging](https://developer.android.com/topic/libraries/architecture/paging/)
    * [Room](https://developer.android.com/topic/libraries/architecture/room)
    * [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
    * [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
- [Android Data Binding](https://developer.android.com/topic/libraries/data-binding/)
- [Retrofit](http://square.github.io/retrofit/) for REST api communication
- [Picasso](http://square.github.io/picasso/) for image loading

## Screenshots
![movies-main](https://user-images.githubusercontent.com/33213229/49940286-757e9100-ff22-11e8-897a-45ba561df250.png)![movies-detail](https://user-images.githubusercontent.com/33213229/49940281-71527380-ff22-11e8-935b-7e2d4138d979.png)