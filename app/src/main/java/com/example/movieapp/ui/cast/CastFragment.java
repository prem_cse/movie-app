package com.example.movieapp.ui.cast;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.movieapp.R;
import com.example.movieapp.databinding.FragmentCastBinding;
import com.example.movieapp.model.Cast;
import com.example.movieapp.model.Credits;
import com.example.movieapp.model.Movie;
import com.example.movieapp.model.MovieDetails;
import com.example.movieapp.ui.info.InfoViewModel;
import com.example.movieapp.ui.info.InfoViewModelFactory;
import com.example.movieapp.utilities.InjectorUtils;

import java.util.ArrayList;
import java.util.List;

import static com.example.movieapp.utilities.Constant.EXTRA_MOVIE;


/**
 * The CastFragment displays all of the cast members for the selected movie.
 */
public class CastFragment extends Fragment {

    /** Tag for a log message */
    public static final String TAG = CastFragment.class.getSimpleName();

    /** Member variable for the list of casts */
    private List<Cast> mCastList;

    /** Member variable for CastAdapter */
    private CastAdapter mCastAdapter;

    /** This field is used for data binding */
    private FragmentCastBinding mCastBinding;

    /** Member variable for the Movie object */
    private Movie mMovie;

    /**
     *  ViewModel for InformationFragment.
     *  MovieDetails data contains the cast data of the movie, and get casts data from the getDetails
     *  method in the InfoViewModel
     */
    private InfoViewModel mInfoViewModel;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the fragment
     */
    public CastFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mCastBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_cast, container, false);
        View rootView = mCastBinding.getRoot();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mCastBinding.rvCast.setLayoutManager(layoutManager);
        mCastBinding.rvCast.setHasFixedSize(true);

        mCastList = new ArrayList<>();
        mCastAdapter = new CastAdapter(mCastList);
        mCastBinding.rvCast.setAdapter(mCastAdapter);
        showOfflineMessage(isOnline());

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMovie = getMovieData();
        setupViewModel(this.getActivity(), mMovie.getId());
    }

    private void setupViewModel(Context context, int movieId) {
        InfoViewModelFactory factory = InjectorUtils.provideInfoViewModelFactory(context, movieId);
        mInfoViewModel = new ViewModelProvider(this, factory).get(InfoViewModel.class);
        mInfoViewModel.getMovieDetails().observe(getViewLifecycleOwner(), new Observer<MovieDetails>() {
            @Override
            public void onChanged(@Nullable MovieDetails movieDetails) {
                if (movieDetails != null) {
                    loadCast(movieDetails);
                }
            }
        });
    }

    /**
     * Gets movie data from the MainActivity.
     */
    private Movie getMovieData() {
        Intent intent = getActivity().getIntent();
        if (intent != null) {
            if (intent.hasExtra(EXTRA_MOVIE)) {
                Bundle b = intent.getBundleExtra(EXTRA_MOVIE);
                mMovie = b.getParcelable(EXTRA_MOVIE);
            }
        }
        return mMovie;
    }

    /**
     * Display the cast of the movie
     */
    private void loadCast(MovieDetails movieDetails) {
        Credits credits = movieDetails.getCredits();
        mCastList = credits.getCast();
        credits.setCast(mCastList);
        mCastAdapter.addAll(mCastList);
    }


    /**
     * Make the offline message visible and hide the cast View when offline
     *
     * @param isOnline True when connected to the network
     */
    private void showOfflineMessage(boolean isOnline) {
        if (isOnline) {
            mCastBinding.tvOffline.setVisibility(View.INVISIBLE);
            mCastBinding.rvCast.setVisibility(View.VISIBLE);
        } else {
            mCastBinding.rvCast.setVisibility(View.INVISIBLE);
            mCastBinding.tvOffline.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Check if there is the network connectivity
     *
     * @return true if connected to the network
     */
    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }
}
